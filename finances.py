import pandas as pd
import matplotlib.pyplot as plt

def generate_membership_pie_chart(csv_file_path):
    # Read the CSV file into a DataFrame
    df = pd.read_csv(csv_file_path)

    # Extract the last row from the DataFrame
    last_row = df.iloc[-1]

    # Extract membership counts
    membership_counts = last_row[["members cornerstone", "members full", "members fullwarden", "members student", "members studentwarden"]]

    # Plot the pie chart
    plt.figure(figsize=(8, 8))
    plt.pie(membership_counts, labels=membership_counts.index, autopct='%1.1f%%', startangle=140)
    plt.title('Current Membership Breakdown')
    plt.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.

    # Save the pie chart to a file
    plt.savefig('current_membership.png')


def generate_income_over_time_bar_chart(csv_file_path):
    # Read the CSV file into a DataFrame
    df = pd.read_csv(csv_file_path)

    # Convert year and month columns to datetime format for proper sorting
    df['year'] = pd.to_datetime(df['year'], format='%Y')
    df['month'] = pd.to_datetime(df['month'], format='%m')

    # Sort the DataFrame by year and month
    df.sort_values(by=['year', 'month'], inplace=True)

    # Calculate income from memberships for each row
    df['fullwarden_income'] = df['members fullwarden'] * 25
    df['full_income'] = df['members full'] * 50
    df['student_income'] = df['members student'] * 13.37
    df['studentwarden_income'] = df['members studentwarden'] * 1
    df['cornerstone_income'] = df['members cornerstone'] * 100

    # Create a new DataFrame for income over time
    income_columns = ["fullwarden_income", "full_income", "student_income", "studentwarden_income", "cornerstone_income"]
    time_income_data = df.groupby(['year', 'month'])[income_columns].sum()

    # Filter out rows where total income is zero
    time_income_data = time_income_data[(time_income_data.T != 0).any()]

    # Check if any non-empty values exist in the relevant columns
    if not time_income_data.empty:
        # Plot the bar chart
        ax = time_income_data.plot(kind='bar', stacked=True, figsize=(12, 6), width=0.5)
        plt.title('Income Over Time')
        plt.xlabel('Year-Month')
        plt.ylabel('Total Income')
        plt.legend(title='Membership Type')

        # Format x-axis labels to show year and month
        x_labels = [f"{year.strftime('%Y')}-{month.strftime('%m')}" for year, month in time_income_data.index]
        ax.set_xticklabels(x_labels, rotation=45, ha='right', fontsize=7)

        # Save the bar chart to a file
        plt.savefig('income_over_time.png')


def generate_combined_balance_line_chart(csv_file_path):
    # Read the CSV file into a DataFrame
    df = pd.read_csv(csv_file_path)

    # Convert year and month columns to datetime format for proper sorting
    df['year'] = pd.to_datetime(df['year'], format='%Y')
    df['month'] = pd.to_datetime(df['month'], format='%m')

    # Sort the DataFrame by year and month
    df.sort_values(by=['year', 'month'], inplace=True)

    # Calculate combined beginning balance
    df['beginning_balance'] = df['pnc main beginning'] + df['pp beginning']

    # Calculate combined ending balance
    df['ending_balance'] = df['pnc main ending'] + df['pp ending']

    # Create a new DataFrame for beginning and ending balance over time
    balance_data = df.groupby(['year', 'month'])[['beginning_balance', 'ending_balance']].sum()

    # Check if any non-empty values exist in the relevant columns
    if not balance_data.empty:
        # Plot the line chart
        ax = balance_data.plot(kind='line', figsize=(12, 6))
        plt.title('Combined Bank Account / PayPal Balance Over Time')
        plt.xlabel('Year-Month')
        plt.ylabel('Balance')
        plt.legend(title='Balance Type')

        # Set the ticks at regular intervals
        tick_interval = 6  # Set ticks every 6 months
        ax.set_xticks(range(0, len(balance_data), tick_interval))

        # Format x-axis labels to show year and month
        x_labels = [f"{year.strftime('%Y')}-{month.strftime('%m')}" for year, month in balance_data.iloc[::tick_interval].index]
        ax.set_xticklabels(x_labels, rotation=45, ha='right')

        # Add grid to the plot
        plt.grid(True)

        # Save the line chart to a file
        plt.savefig('combined_balance_over_time.png')


def generate_in_out_bar_chart(csv_file_path):
    # Read the CSV file into a DataFrame
    df = pd.read_csv(csv_file_path)

    # Calculate combined in/out transactions
    df['in_out'] = df['pnc main deposits'] - df['pnc main deductions']

    # Create a new DataFrame for in/out transactions
    in_out_data = df[['year', 'month', 'in_out']]

    # Check if any non-empty values exist in the in/out column
    if not in_out_data['in_out'].empty:
        # Combine year and month into a single string for x-axis labels
        in_out_data['year_month'] = in_out_data['year'].astype(str) + '-' + in_out_data['month'].astype(str)

        # Plot the bar chart
        ax = in_out_data.plot(kind='bar', x='year_month', y='in_out', figsize=(12, 6), width=0.5, color=['g' if value >= 0 else 'r' for value in in_out_data['in_out']])
        plt.title('In/Out Transactions Over Time')
        plt.xlabel('Year-Month')
        plt.ylabel('Amount')
        plt.legend().remove()

        # Format x-axis labels
        ax.set_xticklabels(in_out_data['year_month'], rotation=45, ha='right')

        # Save the bar chart to a file
        plt.savefig('in_out_transactions.png')

# File path
file_path = "monthlydata_export.csv"

# Generate pie chart for current membership breakdown
generate_membership_pie_chart(file_path)

# Generate bar chart for income over time
generate_income_over_time_bar_chart(file_path)

# Generate line chart for combined bank account / PayPal balance over time
generate_combined_balance_line_chart(file_path)

# Generate bar chart for in/out transactions over time
generate_in_out_bar_chart(file_path)
