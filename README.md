# hive13-financial data

This is an example python script generated using ChatGPT to generate static images of the Hive13 Financial Data. The contents can originally be found [here](https://finance.hive13.org/). To view the chat transcript, click [here](https://chat.openai.com/share/a1eeee5d-b39c-424a-9c84-112e763413f8)

I have included the data set that was used in this content generation as apart of this repository to provide the ability to tune script yourself.